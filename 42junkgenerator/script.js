var	items = [];
var names = ["& and |", "Semicolon ;", "Quotes", "Aggregations 1>&2", "Redirections >> << > <", "Basic Existing Commands", "Wrong Commands", "Double Quotes", "Back Quotes", "Variables", "Backslashes", "Return", "Globbing", "History", "Spaces"];
var nameValues = [
	['&','|'],
	[';'],
	['\''],
	['&', '<', '>', '-', '0', '1', '2', '3', '4', '5', '6', '7','8','9', ' '],
	['>', '<', '>>', '<<'],
	[' echo ', ' cd ', ' cat ', ' setenv ', ' unsetenv ', ' unset ', ' export '],
	[' hahaha ', ' poney ', ' pantoufle ', ' clafoutis '],
	['"'],
	['`'],
	['$USER'],
	['\\'],
	['\n'],
	['[', ']', '{', '}', '*', '?', '-', '!', ' '],
	['!', '?', 'history', '-', '0', '1', '2', '3', '4', '5', '6', '7','8','9', ' '],
	[' ', '\t', '\n']
];

function loadPage() {
	document.getElementById('mainPage').style.visibility = 'visible';
}

function add_input(id) {
	items.push(parseInt(id));
	refresh_list();
}

function refresh_list() {
	var junkList = document.getElementById('junk_list');
	var	itemcount = [];
	junkList.innerHTML = "";
	var len = items.length;
	var index = 0;
	var biggest = 0;
	while (index < len)
	{
		if (!itemcount.includes(items[index]))
			itemcount.push(items[index]);
		index++;
	}
	index = 0;
	var stats = new Array(itemcount.length);
	for(var i = 0; i < itemcount.length; i++) {
    	stats[i] = 0;
	}
	while (index < len)
	{
		stats[itemcount.indexOf(items[index])]++;
		index++;
	}
	for (var i = 0; i < itemcount.length; i++) {
		junkList.innerHTML += '<div id="listElem" style="margin-top: 5px;">';
		junkList.innerHTML += names[itemcount[i] - 1];
		junkList.innerHTML += '  ';
		junkList.innerHTML += parseInt((stats[i] / len) * 100);
		junkList.innerHTML += '%';
		junkList.innerHTML += '</div>';
	}
}

function generate(){
	var generated_text = "";
	var reps = document.getElementById('repetitions').value;
	if (!reps)
		reps = 1000;
	else
		reps = parseInt(reps);
	console.log(reps);
	for (var i = 0; i < reps; i++) {
		var posx = [items[Math.floor(Math.random() * items.length)] - 1];
		var posy = Math.floor(Math.random() * nameValues[posx].length);
		generated_text += nameValues[posx][posy];
	}
	document.getElementById('comment').value = generated_text;
}