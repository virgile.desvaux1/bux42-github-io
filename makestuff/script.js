var Game = {
    stuff : 20,
    stuffClick: 0.5,
    clickCounter: 99,
    Items : {
        LittleStuffMaker : {
            Available : false,
            Count: 0,
            Cost: 30,
            Coef: 0.005,
            Name: "Stuff Maker",
            ClassName: "LittleStuffMaker",
            Desc: "Does stuff",
            RefreshItem: function() {
                document.getElementById(this.ClassName).getElementsByClassName('itemPrice')[0].innerHTML = "Cost : " + this.Cost.toFixed(0);
                document.getElementById(this.ClassName).getElementsByClassName('itemQuantity')[0].innerHTML = "Quantity : " + this.Count;
            },
            BuyOne: function() {
                if (Game.stuff >= this.Cost)
                {
                    Game.stuff -= this.Cost;
                    this.Count++;
                    this.Cost *= 1.1;
                    this.RefreshItem();
                }
            }
        }
    },
    Upgrades : {
        Clicker : {
            Level : 0,
            Name : "Clicking Power",
            Cost : 50,
            Desc : "Click Power Multiplied by Two",
            Upgrade : function() {
                console.log('Upgrading ' + this.Name);
                Game.stuffClick *= 2;
                this.Level++;
            }
        }
    }
}

function unlockItem(Item) {
    Item.Available = true;
    
    var itemName = '<div class="itemName">' + Item.Name + '</div>';
    var itemDesc = '<div class="itemDesc">Description : ' + Item.Desc + '</div>';
    var itemQuantity = '<div class="itemQuantity">Quantity : ' + Item.Count + '</div>';
    var itemBuyButton = '<div class="buyButton" onclick="Game.Items.' + Item.ClassName + '.BuyOne();">Buy One</div>';
    var itemPrice = '<div class="itemPrice">Price: ' + Item.Cost + '</div>';

    var itemHtml = '<div id="' + Item.ClassName + '" class="Item">' + itemName + itemDesc + itemQuantity + itemBuyButton + itemPrice + '</div>';
    $('#Items').html($('#Items').html() + itemHtml);
}

function unlockUpgrade(Upgrade)
{
    Upgrade.Upgrade();
}

function makeStuff() {
    Game.clickCounter++;
    Game.stuff += Game.stuffClick;
    $('#StuffCounter').text(Game.stuff.toFixed(2)  + " Stuff(s)");
    if (Game.clickCounter > 99 && Game.Upgrades.Clicker.Level < 1)
        unlockUpgrade(Game.Upgrades.Clicker);
    if (Game.stuff > 19 && !Game.Items.LittleStuffMaker.Available)
        unlockItem(Game.Items.LittleStuffMaker);
}

setInterval(function(){
    Game.stuff += Game.Items.LittleStuffMaker.Count * Game.Items.LittleStuffMaker.Coef;
    $('#StuffCounter').text(Game.stuff.toFixed(2)  + " Stuff(s)");
    
}, 50);